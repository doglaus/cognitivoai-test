#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 20 21:46:35 2019
@author: douglasoliveira
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import metrics
from sklearn import tree
from sklearn import preprocessing
from sklearn.preprocessing import LabelEncoder
from sklearn import linear_model
from sklearn.model_selection import cross_val_score, cross_val_predict

""" Lendo o dataset """
data = pd.read_csv("./winequality.csv", sep=';') 

""" Na coluna 'alcohol' havia alguns valores que nÃ£o podiam ser convertidos 
para valor numerico, portanto esses valores foram descartados, pois eram apenas
40 amostras, que representam 0,6% do total de amostras
"""
data['alcohol'] = pd.to_numeric(data['alcohol'], errors='coerce')
data.dropna(subset = ['alcohol'], inplace=True)

""" Definindo variavel target """
y = data['quality']

""" Transformando variavel categorica 'type' em numerica """
le = LabelEncoder()
data['type'] = le.fit_transform(data['type'])

""" Avaliando variaveis que mais impactam qualidade"""
x=data.drop('quality', axis=1)
correl_v= list()
for col in x.columns:
    correl_v.append(data[col].corr(y)) 

""" Plotando a correlacao"""
y_pos = np.arange(12)
plt.bar(y_pos, correl_v, align='center', alpha=0.5)
plt.xticks(y_pos, list(x),rotation=60)
plt.ylabel('Correlation with Quality')

"""Normalizando os dados para tornar regressao mais eficiente"""
tmp = data.values #returns a numpy array
min_max_scaler = preprocessing.MinMaxScaler()
data_scaled = min_max_scaler.fit_transform(tmp)
data = pd.DataFrame(data_scaled)

""" Treinando uma regressao linear"""
lm = linear_model.LinearRegression()
model = lm.fit(x, y)

"""Scores (R2) do modelo para cada K-fold """
scores = cross_val_score(model, x, y, cv=5)
print ('Cross-Validated Scores da LinearReg:', np.mean(scores))

"""Score final da previsao usando K-fold """
predictions = cross_val_predict(model, x, y, cv=5)
accuracy = metrics.r2_score(y, predictions)
print ('Cross-Predicted Accuracy da LinearReg:', accuracy)


""" Treinando uma Arvore de decisao, limitando numero de leafs para facilitar
visualizacao da arvore
"""
clf = tree.DecisionTreeClassifier(max_leaf_nodes=10)
clf = clf.fit(x, y)

"""Utilizando K-fold para avaliar os resultados"""
predictions = cross_val_predict(clf, data, y, cv=5)
accuracy = metrics.r2_score(y, predictions)
print ('Cross-Predicted Accuracy da DecTree:', accuracy)

""" Exportando a Arvore de decisao treinada para ser visualizada"""
tree.export_graphviz(clf, out_file='tree.dot') 